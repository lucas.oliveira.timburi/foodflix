import React from "react";
import { Container, LogoContainer, Logo } from "./styles";
import ButtonLink from "../Button";

const Header = () => {
  return (
    <Container>
      <LogoContainer href="/">
        <Logo src={require("../../assets/logo.png")} alt="FoodFlix" />
      </LogoContainer>
      <ButtonLink href="/">Novo vídeo</ButtonLink>
    </Container>
  );
};

export default Header;
