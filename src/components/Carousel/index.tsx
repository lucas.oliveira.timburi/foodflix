import React from "react";
import {
  VideoCardGroupContainer,
  VideoCardList,
  Title,
  ExtraLink,
} from "./styles";
import VideoCard from "./components/VideoCard";

interface VideoCardProps {
  ignoreFirstVideo: boolean;
  category: {
    link?: string;
    title: string;
    color?: string;
    extraLink?: {
      url: string;
      text: string;
    };
    videos: {
      title: string;
      url: string;
    }[];
  };
}

const VideoCardGroup: React.FC<VideoCardProps> = ({
  ignoreFirstVideo,
  category,
}) => {
  const { title, color, extraLink, videos } = category;

  return (
    <VideoCardGroupContainer>
      {title && (
        <>
          <Title color={color}>{title}</Title>
          {extraLink && (
            <ExtraLink href={extraLink.url} target="_blank">
              {extraLink.text}
            </ExtraLink>
          )}
        </>
      )}
      <VideoCardList>
        {videos.map((video, index) => {
          if (ignoreFirstVideo && index === 0) {
            return null;
          }

          return (
            <li key={video.title}>
              <VideoCard
                videoTitle={video.title}
                videoURL={video.url}
                categoryColor={color}
              />
            </li>
          );
        })}
      </VideoCardList>
    </VideoCardGroupContainer>
  );
};

export default VideoCardGroup;
