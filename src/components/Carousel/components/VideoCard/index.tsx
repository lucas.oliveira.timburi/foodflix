import React from "react";
import { VideoCardContainer } from "./styles";
import { getVideoID } from "../../../../services/utils";

interface VideoCardProps {
  videoTitle: string;
  videoURL: string;
  categoryColor?: string;
}

const VideoCard: React.FC<VideoCardProps> = ({
  videoTitle,
  videoURL,
  categoryColor,
}) => {
  const videoId = getVideoID(videoURL);
  const thumbnail = `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`;

  return (
    <VideoCardContainer
      thumbnail={thumbnail}
      href={videoURL}
      target="_blank"
      style={{ borderColor: categoryColor || "green" }}
      title={videoTitle}
    />
  );
};

export default VideoCard;
