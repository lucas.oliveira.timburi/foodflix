import React from "react";
import { FooterBase, FooterButton, Image, Text } from "./styles";

const Footer = () => {
  return (
    <FooterBase>
      <FooterButton href="https://www.alura.com.br/">
        <Image src={require("../../assets/logo.png")} alt="Logo Foodflix" />
      </FooterButton>
      <Text>
        Site feito na #ImersãoReact
        <a href="https://www.alura.com.br/">Alura</a>
      </Text>
    </FooterBase>
  );
};

export default Footer;
