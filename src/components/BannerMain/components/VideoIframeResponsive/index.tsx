import React from "react";
import { VideoContainer, ResponsiveIframe } from "./styles";

interface YouTubeIframeProps {
  youtubeID: string;
}

const YouTubeIframeResponsive: React.FC<YouTubeIframeProps> = ({
  youtubeID,
}) => {
  const videoUrl = `https://www.youtube.com/embed/${youtubeID}?autoplay=0&mute=1`;

  return (
    <VideoContainer>
      <ResponsiveIframe
        title="Titulo do Iframe"
        src={videoUrl}
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </VideoContainer>
  );
};

export default YouTubeIframeResponsive;
