import React from "react";
import { ButtonLink } from "./styles";

interface ButtonProps {
  href: string;
  children: any;
  buttonType?: "primary" | "secondary" | "link";
}

const Header: React.FC<ButtonProps> = ({ children, buttonType }) => {
  return <ButtonLink>{children}</ButtonLink>;
};

export default Header;
