const videoIdPattern = new RegExp(
  /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/
);

export const getVideoID = (url: string) => {
  const videoId = url.replace(videoIdPattern, "$7");
  return videoId;
};
