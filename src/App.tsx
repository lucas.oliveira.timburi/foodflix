import React from "react";
import Header from "./components/Header";
import BannerMain from "./components/BannerMain";
import VideoCardGroup from "./components/Carousel";
import mocked from "./mocked_data.json";

function App() {
  return (
    <div>
      <Header />
      <BannerMain
        videoTitle={mocked.categories[0].videos[0].title}
        videoDescription={"Esse guia vai para quem não dispensa um BOM FAST FOOD, especialmente os da Califórnia, nos Estados Unidos. Assista ao vídeo para ver como são os lanches do In-N-Out, Shake Shack, Five Guys, Chick-fil-A, Polllo Loco e Carl's Jr. "}
        url={mocked.categories[0].videos[0].url}
      />
      <VideoCardGroup ignoreFirstVideo category={mocked.categories[0]} />
    </div>
  );
}

export default App;
